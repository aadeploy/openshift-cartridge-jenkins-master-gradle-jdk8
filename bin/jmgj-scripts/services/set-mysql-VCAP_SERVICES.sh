#!/bin/bash 

# this file is sourced by setting-VCAP_SERVICES.sh which in turn is sourced by main-helper.sh
#       which in turn is sourced by all life-cycle scripts

# its purpose is to check if there is a mysql DB and write the information in vcap format to the file ${sbjrTmpFilename} which is set by the caller

if [[ -n "${OPENSHIFT_MARIADB_DB_URL}" ]];then  
   cart-log  "set-mysql-VCAP_SERVICES.sh true  (${OPENSHIFT_MARIADB_DB_URL})" 
else   
   cart-log  "set-mysql-VCAP_SERVICES.sh false (${OPENSHIFT_MARIADB_DB_URL})" 
fi

# check for MySQL DB environment variables

if [[ -n "${OPENSHIFT_MARIADB_DB_URL}" ]];then  

   cart-log "set-mysql-VCAP_SERVICES.sh OPENSHIFT_MARIADB_DB_URL is set."  

cat > ${sbjrTmpFilename} <<ENDL
"mariadb" : [ {
   "name" : "${OPENSHIFT_APP_NAME}-${OPENSHIFT_MARIADB_DB_USERNAME}",
   "label" : "mariadb",
   "tags" : [ "relational", "Data Store", "mysql" ],
   "credentials" : {
      "jdbcUrl" : "jdbc:mysql://${OPENSHIFT_MARIADB_DB_URL}${OPENSHIFT_APP_NAME}",
      "uri" : "${OPENSHIFT_MARIADB_DB_URL}${OPENSHIFT_APP_NAME}?reconnect=true",
      "name" : "${OPENSHIFT_APP_NAME}",
      "hostname" : "${OPENSHIFT_MARIADB_DB_HOST}",
      "port" : "${OPENSHIFT_MARIADB_DB_PORT}",
      "username" : "${OPENSHIFT_MARIADB_DB_USERNAME}",
      "password" : "${OPENSHIFT_MARIADB_DB_PASSWORD}"
   }}]
ENDL

else
   cart-log "set-mysql-VCAP_SERVICES.sh OPENSHIFT_MARIADB_DB_URL is not set."
fi


