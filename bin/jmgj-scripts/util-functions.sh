#!/bin/bash 

function update_configuration {
  setup-jdk-8
  export_java_home
  reinstall_path
}

function export_java_home() {
  export JAVA_HOME="$OPENSHIFT_JMGJ_DIR/jdk/jdk1.8.0_31"
}

function reinstall_path {
  echo $JAVA_HOME > $OPENSHIFT_JMGJ_DIR/env/JAVA_HOME
  echo "$JAVA_HOME/bin" > $OPENSHIFT_JMGJ_DIR/env/OPENSHIFT_JMGJ_PATH_ELEMENT
}

function cart-log {
  echo  "$( date "+%Y%m%d_%H%M%S" ) $@ " >>  ${OPENSHIFT_JMGJ_LOG_DIR}/cart-JMGJ-log.out
}

function cart-log-message {
  echo  "$( date "+%Y%m%d_%H%M%S" ) $@ " >>  ${OPENSHIFT_JMGJ_LOG_DIR}/cart-JMGJ-log.out
  client_message  "$( date "+%Y%m%d_%H%M%S" ) $@ "
}


function cart-log-error {
  echo "$( date "+%Y%m%d_%H%M%S" ) ERROR $@ " >>  ${OPENSHIFT_JMGJ_LOG_DIR}/cart-JMGJ-log.out
  client_error "$( date "+%Y%m%d_%H%M%S" ) $@ "
  echo "$( date "+%Y%m%d_%H%M%S" ) ERROR $@ " 1>&2
}

function rotate_logs() {
   mv ${OPENSHIFT_JMGJ_LOG_DIR}/java.out ${OPENSHIFT_JMGJ_LOG_DIR}/java.out.$( date "+%Y%m%d_%H%M%S" )
   mv ${OPENSHIFT_JMGJ_LOG_DIR}/java.err.out ${OPENSHIFT_JMGJ_LOG_DIR}/java.err.out$( date "+%Y%m%d_%H%M%S" )
}

function setup-jdk-8() {
  echo "Downloading and Configuring Java 8"
  if [ ! -d "${OPENSHIFT_JMGJ_DIR}/jdk" ]; then
    mkdir -p ${OPENSHIFT_JMGJ_DIR}/jdk
  fi
  
  if [ ! -d "${OPENSHIFT_JMGJ_DIR}/jdk/jdk1.8.0_31" ]; then
    pushd ${OPENSHIFT_JMGJ_DIR}/jdk
    wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u31-b13/jdk-8u31-linux-x64.tar.gz
    tar -zxf jdk-8u31-linux-x64.tar.gz
    rm jdk-8u31-linux-x64.tar.gz
  fi
  echo "Java 8 Downloaded"
}

