#!/bin/bash

# set -e

source $OPENSHIFT_CARTRIDGE_SDK_BASH

source ${OPENSHIFT_JMGJ_DIR}/bin/jmgj-scripts/util-functions.sh

source ${OPENSHIFT_JMGJ_DIR}/bin/jmgj-scripts/setting-java-opt-environment-variables.sh

source ${OPENSHIFT_JMGJ_DIR}/bin/jmgj-scripts/setting-VCAP_SERVICES.sh

