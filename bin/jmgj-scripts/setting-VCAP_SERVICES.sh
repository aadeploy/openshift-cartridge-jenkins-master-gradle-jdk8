#!/bin/bash

# this file is sourced by main-helper.sh which in turn is sourced by all life-cycle scripts

# its purpose is to execute all service check scripts under the services directory to simulate 
# the appropriate VCAP_SERVICES content for that service
#
# a better option would be to extend the spring-cloud framework for openshift (which is what the framework intended) 

cart-log 'setting-VCAP_SERVICES.sh' called

function appendCommaIfNecessary() {
   cart-log "calling appendCommaIfNecessary"

   
   if [[ -n "${VCAP_SERVICES}" ]];then

      cart-log "appending comma appendCommaIfNecessary"
      
      VCAP_SERVICES="${VCAP_SERVICES},"
   
   fi

      cart-log "done appendCommaIfNecessary"
}

function export_java_home() {
  export JAVA_HOME="$OPENSHIFT_JMGJ_DIR/jdk/jdk1.8.0_31"
}


# setting a CloudFoundry compatible environment 
export VCAP_SERVICES=""


export sbjrTmpFilename=$(mktemp)

for serviceFile in ${OPENSHIFT_JMGJ_DIR}/bin/jmgj-scripts/services/*; do

   # empty file
   > ${sbjrTmpFilename}

   cart-log "calling '${serviceFile}'"
    
   . ${serviceFile} 
   
   appendCommaIfNecessary
   
   # append file content to VCAP_SERVICES variable
   VCAP_SERVICES="${VCAP_SERVICES}$( cat ${sbjrTmpFilename} )"

done


VCAP_SERVICES="{${VCAP_SERVICES}}"

export VCAP_APP_HOST=${OPENSHIFT_JMGJ_IP} 
export VCAP_APP_PORT=${OPENSHIFT_JMGJ_HTTP_PORT}

cat > ${sbjrTmpFilename} <<ENDL
{
   "limits" : {
      "mem" : 1024,
      "disk" : 1024,
      "fds" : 16384
   },
   "application_version" : "12345678-1234-1234-1234-123456789012",
   "application_name" : "${OPENSHIFT_APP_NAME}",
   "application_uris" : [ "${OPENSHIFT_APP_DNS}" ],
   "version" : "12345678-1234-1234-1234-123456789012",
   "name" : "${OPENSHIFT_APP_NAME}",
   "space_name" : "development",
   "space_id" : "spaceid12345678901234567890123456789",
   "uris" : [ "${OPENSHIFT_APP_DNS}" ],
   "users" : null,
   "application_id" : "appid1234567890123456789012345678901",
   "instance_id" : "instanceid1234567890123456789012",
   "instance_index" : 0,
   "host" : "${OPENSHIFT_JMGJ_IP}",
   "port" : "${OPENSHIFT_JMGJ_HTTP_PORT}",
   "started_at" : "2014-06-15 00:33:09 +0000",
   "started_at_timestamp" : 1402792389,
   "start" : "2014-06-15 00:33:09 +0000",
   "state_timestamp" : 1402792389
}
ENDL

export VCAP_APPLICATION=$( cat ${sbjrTmpFilename} )

rm -rf ${sbjrTmpFilename}
